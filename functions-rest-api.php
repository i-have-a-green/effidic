<?php

add_action(
	'rest_api_init',
	function() {
		register_rest_route(
			'ihag',
			'download-use-case',
			array(
				'methods'               => 'POST', 
				'callback'              => 'ihag_register_download_use_case',
			)
		);
		register_rest_route( 'ihag', 'register-newsletter',
			array(
			'methods'               => 'POST', 
			'callback'              => 'ihag_register_rewsletter'
			)
		);
	} 
);
function ihag_register_download_use_case( WP_REST_Request $request ) {
	if ( ihag_check_nonce() ) {
		$params = $request->get_params();
		require_once __DIR__ . '/vendor/autoload.php'; 
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey( 'api-key', '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');
		$api_instance = new SendinBlue\Client\Api\ContactsApi( new GuzzleHttp\Client(),	$config );
		$create_contact = new \SendinBlue\Client\Model\CreateContact();
		$create_contact['email'] = sanitize_email( $params['email-download-use-case'] );
		$create_contact['listIds'] = array( 14 );
		try {
			$result = $api_instance->createContact( $create_contact );
		} catch ( Exception $e ) { 
			
		}
		if ( get_field( 'use_case_file', $params['ref-id'] ) ) {
			return new WP_REST_Response( get_field( 'use_case_file', $params['ref-id'] ), 200 );
		} else {
			return new WP_REST_Response( '', 500 ); 
		}
	}
	return new WP_REST_Response( 'BAD NONCE', 401 );
}


function ihag_register_rewsletter(WP_REST_Request $request){
    if ( ihag_check_nonce() ) {
        $params = $request->get_params();
        require_once(__DIR__ . '/vendor/autoload.php');
        $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
        $apiInstance = new SendinBlue\Client\Api\ContactsApi(
            new GuzzleHttp\Client(),
            $config
        );
        $createContact = new \SendinBlue\Client\Model\CreateContact(); // Values to create a contact
        $createContact['email'] = sanitize_email($params['email-newsletter']);
        $createContact['listIds'] = array(13);
        try {
            $result = $apiInstance->createContact($createContact);
            $bool = true;
        } catch (Exception $e) {
            $bool = false;
        }
        if($bool){
            return new WP_REST_Response( 'L’inscription à la newsletter est envoyé !', 200 );    
        }
        return new WP_REST_Response( 'Cette adresse est déjà inscrite.', 200 );
    }
    return new WP_REST_Response( 'BAD NONCE', 401 );
}