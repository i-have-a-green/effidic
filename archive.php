<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

get_header();
?>
	<main id="primary" class="site-main">
		<div class="entry-content">
			<?php echo get_the_content( null, false, get_option( 'page_for_posts' ) ); ?>
			<?php 
			if ( have_posts() ) :
				$taxo  = 'post_tag';
				$terms = get_terms( array( 'taxonomy' => $taxo ) );
				?>
				<div class="mobile-filters">
					<span id="toggle-filters">Filtres</span>
					<div id="nav-tags" class="nav-tags">
						<a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>"  class="<?php echo esc_attr( ( empty( get_queried_object()->term_id ) ) ? 'active' : '' ); ?>"><?php esc_attr_e( 'Tous', 'ihag' ); ?></a>
						<?php
						foreach ( $terms as $term_post_tag ) :
							?>
							<a href="<?php echo esc_url( get_term_link( $term_post_tag ) ); ?>" class="<?php echo esc_attr( ( get_queried_object()->term_id === $term_post_tag->term_id ) ? 'active' : '' ); ?>"><?php echo esc_attr( $term_post_tag->name ); ?></a>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="postContainer">
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/archive/archive-post', '' );
					endwhile;
					?>
					<div class="site__navigation">
						<div class="site__navigation__prev">
							<?php previous_posts_link( '‹ Publications précédentes' ); ?>
						</div>
						<div class="site__navigation__next">
							<?php next_posts_link( 'Publications suivantes ›' ); ?> 
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php dynamic_sidebar( 'footer-blog-widget' ); ?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
