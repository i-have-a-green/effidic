<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ihag
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Aller directement au contenu', 'ihag' ); ?></a>
		<a class="skip-link screen-reader-text" href="#site-navigation"><?php esc_html_e( 'Aller à la navigation', 'ihag' ); ?></a>
		<header id="masthead" class="site-header">
			<div class="site-branding">
				<?php the_custom_logo(); ?>

				<?php if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php 
				endif;

				$ihag_description = get_bloginfo( 'description', 'display' );
				if ( $ihag_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $ihag_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<div class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.86 23.49"><defs><style>.g{fill:#1e2544;}</style></defs><g id="b"><g id="c"><g><path id="d" class="g" d="M23.5,6.73c-.85,0-1.67-.32-2.29-.9-.05-.07-.11-.13-.18-.18-.04-.04-.08-.09-.12-.14h-.01c-2.34-2.12-5.89-2.11-8.22,0h0c-.06,.07-.12,.14-.18,.21l-.02,.02c-1.31,1.32-3.44,1.32-4.76,.01-1.32-1.31-1.32-3.44-.01-4.76s3.44-1.32,4.76-.01c.08,.08,.15,.16,.22,.24h0c1.13,1.01,2.59,1.57,4.1,1.57,1.52,0,2.98-.55,4.12-1.56h.01c1.18-1.44,3.3-1.64,4.74-.46,1.43,1.18,1.63,3.3,.45,4.74-.64,.77-1.59,1.22-2.59,1.22"/><path id="e" class="g" d="M23.5,23.48c-.85,0-1.67-.32-2.29-.9-.05-.07-.11-.13-.18-.18-.04-.04-.08-.09-.12-.14h-.01c-2.34-2.12-5.89-2.11-8.22,0h0c-.06,.07-.12,.14-.18,.21l-.02,.02c-1.31,1.32-3.45,1.32-4.76,0-1.32-1.31-1.32-3.45,0-4.76s3.45-1.32,4.76,0c.08,.08,.15,.15,.21,.24h0c1.13,1.01,2.59,1.57,4.1,1.57,1.52,0,2.98-.55,4.12-1.56h0c1.18-1.44,3.3-1.64,4.74-.46,1.43,1.18,1.63,3.3,.45,4.74-.64,.77-1.59,1.22-2.59,1.22"/><path id="f" class="g" d="M16.79,15.1c-.85,0-1.67-.32-2.29-.9-.05-.07-.11-.13-.18-.18-.04-.04-.08-.09-.12-.14h0c-2.34-2.12-5.89-2.11-8.22,0h0c-.06,.07-.12,.14-.18,.21l-.02,.02c-1.31,1.32-3.45,1.32-4.76,0-1.32-1.31-1.32-3.45,0-4.76s3.45-1.32,4.76,0c.08,.08,.15,.15,.21,.24h0c1.13,1.01,2.59,1.57,4.1,1.57,1.52,0,2.98-.55,4.12-1.56h.01c1.18-1.44,3.3-1.64,4.74-.46s1.63,3.3,.45,4.74c-.64,.77-1.59,1.22-2.59,1.22"/></g></g></g></svg>
			</div>
			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->
