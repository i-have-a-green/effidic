<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

?>

<article id="refrence-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php ihag_post_thumbnail(); ?>

	<div class="content-ref">
		<header class="entry-header">
		<?php
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		?>
		</header><!-- .entry-header -->
		<?php
		$terms = get_the_terms( $post, 'reference-cat' );
		if ( $terms ) {
			echo '<div class="ref-tags">';
			foreach ( $terms as $term_cat ) :
				?>
					<a href="<?php echo esc_url( get_term_link( $term_cat ) ); ?>"><?php echo esc_attr( $term_cat->name ); ?></a>
				<?php 
			endforeach;
			echo '</div>';
		}

		the_excerpt();
		?>
	</div>
	<div class="btn-container">
		<a class="wp-block-button" href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php esc_attr_e( 'Voir le cas client', 'ihag' ); ?></a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
