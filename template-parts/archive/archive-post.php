<?php
/**
 * Template part for displaying archive posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumbnail">
		<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php the_post_thumbnail( '380-214' ); ?></a>
	</div>

	<div class="content">
		<header class="entry-header">
			<?php
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			?>
		</header><!-- .entry-header -->
		<div class="absolute-container">
			<time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php echo get_the_date( 'd/m/y' ); ?></time>
			<?php
			$terms = get_the_terms( $post, 'post_tag' );
			if ( $terms ) {
				echo '<div class="post-tags">';
				foreach ( $terms as $term_post_tag ) :
					?>
						<a href="<?php echo esc_url( get_term_link( $term_post_tag ) ); ?>"><?php echo esc_attr( $term_post_tag->name ); ?></a>
					<?php 
				endforeach;
				echo '</div>';
			}
			?>
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div><!-- .entry-content -->
			<a class="wp-block-button" href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php esc_attr_e( 'Lire l\'article', 'ihag' ); ?></a>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
