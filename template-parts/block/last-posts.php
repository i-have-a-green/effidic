<?php
/**
 * Block Name: Bloc last-posts
 */

?>
<section class="last-posts">
	<h2>Découvrez notre<br>actualité</h2>
	<?php
	$lastposts = get_posts(
		array(
			'posts_per_page' => 3,
			'post_status'    => 'publish',
			'meta_query'     => array(
				array(
					'key'   => 'display_home',
					'value' => '1', 
				),
			),
		) 
	);
	?>
	<div class="post-container">
		<?php
		global $post;
		if ( $lastposts ) { 
			foreach ( $lastposts as $post ) :
				setup_postdata( $post );
				get_template_part( 'template-parts/archive/archive-post', '' );
			endforeach; 
			wp_reset_postdata();
		}
		?>
	</div><!-- .post-container -->
	<a class="wp-block-button more-posts button" href="<?php echo esc_url( get_post_type_archive_link( 'post' ) ); ?>"><?php esc_attr_e( "Voir plus d'articles", 'ihag' ); ?></a>
</section>
