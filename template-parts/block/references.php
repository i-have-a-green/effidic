<?php
/**
 * Block Name: Bloc references
 */

?>
<section class="references ">
	<h2><?php esc_attr_e( 'Ils nous font confiance', 'ihag' ); ?></h2>
	<?php
	$references = get_posts(
		array(
			'posts_per_page' => 3,
			'orderby'        => 'rand',
			'post_status'    => 'publish',
			'post_type'      => 'reference',
			'meta_query'     => array(
				array(
					'key'   => 'display_home',
					'value' => '1',
				),
			),
		) 
	);
	?>
	<div class="post-container">
		<?php
		if ( $references ) {
			global $post;
			foreach ( $references as $post ) :
				setup_postdata( $post );
				get_template_part( 'template-parts/content', 'reference' );
			endforeach; 
			wp_reset_postdata();
		}
		?>
	</div><!-- .post-container -->

	<a class="wp-block-button button" href="<?php echo esc_url( get_post_type_archive_link( 'reference' ) ); ?>"><?php esc_attr_e( 'Voir tous les cas clients', 'ihag' ); ?></a>
</section>
