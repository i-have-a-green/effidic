<?php
/**
 * Block Name: Bloc newsletter
 */

?>
<form id="register-newsletter" name="register-newsletter">
	<label for="email-newsletter">Adresse email</label>
	<input type="email" id="email-newsletter" name="email-newsletter" required>
	<input type="hidden" id="email-newsletter-hp" name="email-newsletter-hp" value="">
	<div class="rgpdNews" style="margin-top: 1rem;">
		* <label for="rgpdNews"><input type="checkbox" id="rgpdNews" name="rgpdNews" required oninvalid="this.setCustomValidity('Le consentement est obligatoire pour s\'inscrire à la newsletter.')" oninput="this.setCustomValidity('')" > Je reconnais avoir pris connaissance et accepte les <a href="<?php echo esc_url(get_privacy_policy_url());?>"> Mentions légales</a></label>
	</div>
	<input type="submit" id="register-newsletter-btn" name="register-newsletter-btn" class="button" value="s'inscrire" >
</form>
<p id="register-newsletter-ok" style="display:none"></p>
