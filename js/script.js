// Form Cas client

document.addEventListener( 'DOMContentLoaded', function() {
	if ( document.forms.namedItem( 'download-use-case' ) ) {
		const form = document.forms.namedItem( 'download-use-case' );
		form.addEventListener( 'submit', function( e ) {
			e.preventDefault();
			e.stopPropagation();
			document.getElementById( 'download-use-case-btn' ).disabled = true;
			fetch( resturl + 'download-use-case', {
				method: 'POST',
				body: new FormData( form ),
				headers: { 'X-WP-Nonce': wpApiSettings.nonce },
				cache: 'no-cache',
			} )
				.then(
					function( response ) {
						if ( response.status !== 200 ) {
							console.log( response.status ); return;
						}
						response.json().then( function( data ) {
							document.getElementById( 'download-use-case-btn' ).disabled = false;
							const anchor = document.createElement( 'a' );
							anchor.href = data;
							anchor.download = '';
							anchor.click();
						} );
					}
				)
				.catch( function( err ) {
					console.log( 'Fetch Error :-S', err );
				} );
		} );
	}

	if(document.forms.namedItem("register-newsletter")){
		let form = document.forms.namedItem("register-newsletter");
		form.addEventListener('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();
			document.getElementById('register-newsletter-btn').disabled = true;
			fetch(resturl + 'register-newsletter', {
				method: 'POST',
				body: new FormData(form),
				headers: {'X-WP-Nonce': wpApiSettings.nonce},
				cache: 'no-cache',
			})
			.then(
				function(response) {
					
					if (response.status !== 200) { 
						console.log(response.status); return;
					}
					
					response.json().then(function(data) {
						document.getElementById('register-newsletter-ok').innerHTML = data;
						document.getElementById('register-newsletter-btn').disabled = false;
						document.getElementById("register-newsletter-ok").style.display = 'block';
					});
				}
			)
			.catch(function(err) {
				console.log('Fetch Error :-S', err);
			});
		});
	}

});

// Toggle mobile filters

if ( document.getElementById( 'toggle-filters' ) ) {
	document.getElementById( 'toggle-filters' ).addEventListener( 'click', function() {
		document.getElementById( 'nav-tags' ).classList.toggle( 'filters-reveal' );
	} );
}
