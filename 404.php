<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link (https://codex.wordpress.org/Creating_an_Error_404_Page)
 *
 * @package Ihag 
 */

get_header();
?>

<main id="primary" class="site-main">
	<section class="error-404 not-found">

		<div class="wp-block-effidic-section-bg alignfull">
			<div class="wp-container-3 wp-block-group alignwide">
				<div class="wp-block-group__inner-container">
					<div class="wp-block-columns">
						<div class="wp-block-column" style="flex-basis:80%">
							<h1 class="page-title"><?php esc_html_e( 'Cette page n\'existe plus, retournons à l\'accueil', 'ihag' ); ?></h1>
							<div class="wp-container-2 wp-block-buttons">
								<div class="wp-block-button is-style-red">
									<a class="wp-block-button__link" href="<?php echo esc_url( get_home_url() ); ?>">Retourner à l'accueil</a>
								</div>
							</div>
						</div>
						<div class="wp-block-column" style="flex-basis:20%">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- .error-404 -->

</main><!-- #main -->

<?php
get_footer();
