<?php

add_action( 'acf/init', 'ihag_init_blocs' );
function ihag_init_blocs() {
	if ( function_exists( 'acf_register_block_type' ) ) {
		acf_register_block_type(
			array(
				'name'                  => 'last-posts',
				'title'                 => __( 'Derniers articles', 'ihag' ),
				'description'           => __( 'Affiche les 2 derniers rticles du blog', 'ihag' ),
				'placeholder'           => __( 'Derniers articles', 'ihag' ),
				'render_template'       => 'template-parts/block/last-posts.php',
				'mode'                  => 'preview',
				'icon'                  => 'admin-post',
				'keywords'              => array( 'actualité', 'derniers', 'articles', 'post', 'blog' ),
				'supports'              => array(
					'align' => false,
					'mode'  => false,
				),
			)
		);
		acf_register_block_type(
			array(
				'name'                  => 'last-reference',
				'title'                 => __( 'Cas clients', 'ihag' ),
				'description'           => __( 'Affiche 3 cas client en aléatoire', 'ihag' ),
				'placeholder'           => __( 'Cas clients', 'ihag' ),
				'render_template'       => 'template-parts/block/references.php',
				'mode'                  => 'preview',
				'icon'                  => 'portfolio',
				'keywords'              => array( 'reference', 'références' ),
				'supports'              => array(
					'align' => false,
					// 'multiple' => false,
					'mode'  => false,
				),
			)
		);

		acf_register_block_type(
			array(
				'name'                  => 'form-newsletter',
				'title'                 => __( 'Formulaire Newsletter', 'ihag' ),
				'description'           => __( 'Formulaire Newsletter', 'ihag' ),
				'render_template'       => 'template-parts/block/newsletter.php',
				'mode'                  => 'preview',
				'icon'                  => 'email',
				'keywords'              => array( 'Form', 'Formulaire', 'newsletter' ),
				'parent'                => array( 'effidic/newsletter' ),
				'supports'              => array(
					'align' => false,
					'mode'  => false,
				),
			)
		);

		acf_register_block_type(
			array(
				'name'                  => 'share-post',
				'title'                 => __( 'Partage d\'article', 'ihag' ),
				'description'           => __( 'Partage d\'article', 'ihag' ),
				'render_template'       => 'template-parts/block/share.php',
				'mode'                  => 'preview',
				'icon'                  => 'email',
				'keywords'              => array( 'partage', 'twitter', 'linkedin', 'post', 'aricle' ),
				'enqueue_script' 		=> get_template_directory_uri() . '/js/share.js',
				'supports'              => array(
					'align' => false,
					'mode'  => false,
				),
			)
		);





		
	}
}
