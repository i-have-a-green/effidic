<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ihag
 */

get_header();
?>
	<main id="primary" class="site-main"> 
		<div class="entry-content">
			<div class="wp-block-effidic-first-section alignfull">
				<div class="wp-block-columns aligndefault">
					<div class="wp-block-column">
						<h1 class="h2-like"><?php the_title(); ?></h1>
					</div>
					<div class="wp-block-column"></div>
				</div>
			</div>
			<div class="wp-block-effidic-section-bg alignfull is-style-fg-io">
				<div class="wp-container-1 wp-block-group has-white-color has-text-color">
					<div class="wp-block-group__inner-container">
						<div class="wp-block-columns">
							<div class="wp-block-column" style="flex-basis:33.33%">
								<?php the_post_thumbnail( '300-300' ); ?>	
							</div>
							<div class="wp-block-column glossary-content" style="flex-basis:66.66%">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="nav_container alignfull">
				<a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>"><?php esc_attr_e( 'Retour au glossaire', 'ihag' ); ?></a>
			</div>
			<?php dynamic_sidebar( 'newsletter-post-widget' ); ?>
		</div>
	</main><!-- #main -->

<?php

get_footer();
