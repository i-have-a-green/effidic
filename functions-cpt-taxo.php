<?php
/**
 * Declarartion des CPT et TAXO.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ihag
 */
function ihag_unregister_taxonomy() {
	// register_taxonomy( 'post_tag', array() );
	register_taxonomy( 'category', array() );
}
add_action( 'init', 'ihag_unregister_taxonomy' );


add_action( 'init', 'ihag_custom_post_property' );
function ihag_custom_post_property() {
	register_post_type(
		'reference', 
		array(
			'labels'             => array(
				'name'          => __( 'Références', 'ihag' ), /* This is the Title of the Group */
				'singular_name' => __( 'Référence', 'ihag' ), /* This is the individual type */
			), /* end of arrays */
			'menu_position'      => 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon'          => 'dashicons-portfolio', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'cas-clients' ),
			'show_in_rest'       => false,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
			
		) /* end of options */
	);

	

	$args = array(
		'label'        => __( 'Catégorie', 'ihag' ),
		'hierarchical' => true,
	);
	register_taxonomy( 'reference-cat', 'reference', $args );
	

	
	// CPT Glossaire
	register_post_type(
		'glossary', 
		array(
			'labels'             => array(
				'name'          => __( 'Glossaire', 'ihag' ), /* This is the Title of the Group */
				'singular_name' => __( 'Glossaire', 'ihag' ), /* This is the individual type */
			), /* end of arrays */
			'menu_position'      => 19, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon'          => 'dashicons-portfolio', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'glossaire' ),
			'show_in_rest'       => false,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'revisions', 'excerpt'  ),
		
		) /* end of options */
	);

	$args = array(
		'label'        => __( 'Lettre', 'ihag' ),
		'hierarchical' => true,
	);
	register_taxonomy( 'glossary-letter', 'glossary', $args );
}
