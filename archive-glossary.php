<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

get_header();
?>
	<main id="primary" class="site-main">
		<div class="entry-content">
			<?php if ( have_posts() ) : ?>
				<?php dynamic_sidebar( 'header-glossary-widget' ); ?>
				<div class="nav-tags">
					<?php
					$ihag_terms = get_terms( array( 'taxonomy' => 'glossary-letter' ) );
					foreach ( $ihag_terms as $ihag_term ) :
						?>
						<a href="#letter-<?php echo esc_attr( $ihag_term->slug ); ?>"><?php echo esc_attr( $ihag_term->name ); ?></a>
					<?php endforeach; ?>
				</div>
				<div class="glossary-container"> 
					<?php foreach ( $ihag_terms as $ihag_term ) : ?>
						<div id="letter-<?php echo esc_attr( $ihag_term->slug ); ?>" <?php post_class(); ?>>
							<header class="entry-header">
								<?php echo '<h2 id="' . esc_attr( $ihag_term->term_id ) . '">' . esc_attr( $ihag_term->name ) . '</h2>'; ?>
								<hr class="align-left">
							</header> 
							<div class="letter-container">
								<?php 
								$args = array(
									'numberposts' => -1,
									'post_type'   => 'glossary',
									'tax_query'   => array(
										array(
											'taxonomy' => 'glossary-letter',
											'field'    => 'id',
											'terms'    => $ihag_term->term_id,
										),
									),
								);
								global $post;
								$ihag_posts = get_posts( $args );
								foreach ( $ihag_posts as $post ) :
									setup_postdata( $post );
									?>
									<div>
										<?php
											echo '<a href="' . esc_url( get_the_permalink() ) . '">' . esc_attr( get_the_title() ) . '</a>';
											the_excerpt(); 
										?>
									</div>
									<?php 
								endforeach; 
								wp_reset_postdata();
								?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<?php dynamic_sidebar( 'newsletter-post-widget' ); ?>
		</div>
	</main>

<?php
get_footer();
?>
