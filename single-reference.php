<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ihag
 */

get_header();
?>
	<main id="primary" class="site-main"> 
		<div class="entry-content">
			<div class="wp-block-effidic-first-section alignfull">
				<div class="wp-block-columns aligndefault">
					<div class="wp-block-column">
						<h1 class="h2-like"><?php the_title(); ?></h1>
					</div>
					<div class="wp-block-column"></div>
				</div>
			</div>
			<div class="wp-block-effidic-section-bg alignfull is-style-fo-ibh">
				<div class="wp-container-1 wp-block-group has-white-color has-text-color">
					<div class="wp-block-group__inner-container">
						<div class="wp-block-columns">
							<div class="wp-block-column full-img" style="flex-basis:30%; margin-right: 1rem;">
								<?php the_post_thumbnail( '300-9999' ); ?>	
							</div>
							<div class="wp-block-column" style="flex-basis:70%">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php if ( get_field( 'use_case_file' ) ) : ?>
				<div class="wp-block-effidic-section-bg alignfull is-style-fb-ib download-cta">
					<div class="wp-block-group__inner-container">
						<div class="wp-block-columns">
							<div class="wp-block-column" style="flex-basis:60%">
								<h2>Découvrez ce cas client</br>en détail !</h2>
								<p>Téléchargez ce cas client</br>Et découvrez tous les détails de notre prestation</p>
								<form id="download-use-case" name="download-use-case">
									<input type="hidden" name="ref-id" value="<?php the_ID(); ?>">
									<label for="email-download-use-case">Adresse email</label>
									<input placeholder="Insérez votre e-mail" type="email" id="email-download-use-case" name="email-download-use-case" required>
									<input type="hidden" id="email-download-use-case-hp" name="email-download-use-case-hp" value="">
									<div class="rgpd" style="margin-top: 1rem;">
										* <label for="rgpd"><input type="checkbox" id="rgpd" name="rgpd" required oninvalid="this.setCustomValidity('Le consentement est obligatoire pour télécharger ce cas client.')" oninput="this.setCustomValidity('')" > Je reconnais avoir pris connaissance et accepte les <a href="<?php echo esc_url(get_privacy_policy_url());?>"> Mentions légales</a></label>
									</div>
									<input type="submit" id="download-use-case-btn" name="download-use-case-btn" class="button" value="Télécharger le cas client" >
								</form>
							</div>
							<div class="wp-block-column" style="flex-basis:40%">
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="nav_container alignfull">
				<div class="post__navigation">
					<div class="nav_prev">
						<?php previous_post_link( '%link', __( '< Cas precédent', 'ihag' ) ); ?> 
					</div>
					<a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>"><?php esc_attr_e( 'Retour au cas clients', 'ihag' ); ?></a>
					<div class="nav-next">
						<?php next_post_link( '%link', __( 'Cas suivant >', 'ihag' ) ); ?>
					</div>
				</div>
			</div>
			<?php dynamic_sidebar( 'footer-single-references-widget' ); ?>
		</div>
	</main><!-- #main -->

<?php

get_footer();
