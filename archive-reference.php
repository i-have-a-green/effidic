<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */

get_header();
$ihag_current_term_id = get_queried_object()->term_id;
?>
	<main id="primary" class="site-main">
		<div class="entry-content">
			<?php dynamic_sidebar( 'header-references-widget' ); ?>
			<?php if ( have_posts() ) : ?>
				<div class="mobile-filters">
					<span id="toggle-filters">Filtres</span>
					<div id="nav-tags" class="nav-tags">
						<a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>" class="<?php echo ( empty( $ihag_current_term_id ) ) ? 'active' : ''; ?>"><?php esc_attr_e( 'Tous', 'ihag' ); ?></a>
						<?php
						$ihag_terms = get_terms( array( 'taxonomy' => 'reference-cat' ) );
						if ( $ihag_terms ) :
							foreach ( $ihag_terms as $ihag_term ) : 
								?>
								<a href="<?php echo esc_url( get_term_link( $ihag_term ) ); ?>" class="<?php echo esc_attr( ( $ihag_current_term_id === $ihag_term->term_id ) ? 'active' : '' ); ?>"><?php echo esc_attr( $ihag_term->name ); ?></a>
								<?php 
							endforeach; 
						endif;
						?>
					</div>
				</div>
				<div class="refContainer">
					<?php 
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content-reference', '' );
					endwhile; 
					?>
				</div>		
				<div class="site__navigation">
						<div class="site__navigation__prev">
							<?php previous_posts_link( '‹ Cas clients précédents' ); ?>
						</div>
						<div class="site__navigation__next">
							<?php next_posts_link( 'Cas clients suivants ›' ); ?> 
						</div>
				</div>
			<?php endif; ?>
			<?php dynamic_sidebar( 'footer-references-widget' ); ?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
