<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ihag
 */

get_header();
?>
	<main id="primary" class="site-main"> 
		<div class="entry-content">
			<div class="wp-block-effidic-first-section alignfull">
				<div class="wp-block-columns aligndefault">
					<div class="wp-block-column" style="flex-basis:70%">
						<h1 class="h2-like"><?php the_title(); ?></h1>
					</div>
					<div class="wp-block-column"></div>
				</div>
			</div>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
			<div class="nav_container alignfull">
				<div class="post__navigation">
					<div class="nav_prev">
						<?php previous_post_link( '%link', __( '< Article precédent', 'ihag' ) ); ?> 
					</div>
					<a href="<?php echo esc_url( get_post_type_archive_link( get_post_type() ) ); ?>"><?php esc_attr_e( 'Retour aux articles', 'ihag' ); ?></a>
					<div class="nav-next">
						<?php next_post_link( '%link', __( 'Article suivant >', 'ihag' ) ); ?>
					</div>
				</div>
			</div>
			<?php dynamic_sidebar( 'footer-single-post-widget' ); ?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
